FROM ubuntu:latest

RUN apt-get -y update; \
    apt-get -y upgrade; \
    apt-get -y install apt-utils; \
	apt-get -y install curl nano;
RUN curl -L -O https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-7.9.2-amd64.deb; \
    dpkg -i filebeat-7.9.2-amd64.deb ;

ENTRYPOINT tail -f /var/log/faillog